'use strict'

import 'foundation-sites'
import * as $ from 'jquery'

let scrolled, fixed

$(document).foundation()

$( function () {

    let offset          = $('.top-bar').outerHeight(),
        menuToggle      = $('.menu-toggle'),
        backendToggle   = $('.menu-toggle-backend'),
        overlay         = $('.overlay')

    // fixed menu on scroll
    let scroll = function () {
        scrolled = $(window).scrollTop()

        if (scrolled > 0 && !fixed || scrolled <= 0 && fixed) {
            $('body').toggleClass('fixed')
            fixed = !fixed
        }
    }

    // Add scroll listener
    $(window).scroll( scroll )

    // Open close menu
    menuToggle.click( function () {
        $('body').toggleClass('menu-open')

        $('body').is('.backend-open') ? $('body').removeClass('backend-open') : 0
    })

    // Open close menu
    backendToggle.click( function () {
        $('body').toggleClass('backend-open')

        $('body').is('.menu-open') ? $('body').removeClass('menu-open') : 0
    })

    overlay.click( function () {
        $('body').is('.menu-open') ? $('body').removeClass('menu-open') : 0
        $('body').is('.backend-open') ? $('body').removeClass('backend-open') : 0
    })

})
